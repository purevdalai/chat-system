package chinggis.systems.controller;

import chinggis.systems.model.service.ChatEntryService;
import chinggis.systems.model.service.ChatEntryServiceImpl;
import chinggis.systems.model.service.UserService;
import chinggis.systems.model.service.UserServiceImpl;
import chinggis.systems.service.MainService;
import chinggis.systems.service.ServerSocketServiceImpl;

public class ServerMain {

	public static void main(String[] args) {

		final int port = 1111;
		ServerController controller = new ServerController();
		MainService service = new ServerSocketServiceImpl(port, controller);
		UserService userService = new UserServiceImpl();
		ChatEntryService chatEntryService = new ChatEntryServiceImpl();
		controller.setService(service);
		controller.setUserService(userService);
		controller.setChatEntry(chatEntryService);
		controller.run();
		
	}
}