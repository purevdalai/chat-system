package chinggis.systems.controller;

import org.json.simple.JSONObject;

import chinggis.systems.model.entity.Chat;
import chinggis.systems.model.entity.ChatEntry;
import chinggis.systems.model.entity.User;
import chinggis.systems.model.service.ChatEntryService;
import chinggis.systems.model.service.ChatService;
import chinggis.systems.model.service.UserService;
import chinggis.systems.service.LogService;
import chinggis.systems.service.LogServiceImpl;
import chinggis.systems.service.MainService;

public class ServerController {

	UserService userService;
	ChatEntryService chatEntryService;
	MainService service;
	LogService logService;

	public ServerController() {
	}

	/**
	 * UserService -н обьектийг үүсгэх
	 * 
	 * @param userService
	 *            UserService обьект
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * chatEntryService -н обьектийг үүсгэх
	 * 
	 * @param chatEntryService
	 *            ChatEntryService обьект
	 */
	public void setChatEntry(ChatEntryService chatEntryService) {
		this.chatEntryService = chatEntryService;
	}

	/**
	 * MainService -н обьектийг үүсгэх
	 * 
	 * @param mainService
	 *            MainService обьект
	 */
	public void setService(MainService service) {
		this.service = service;
	}

	/**
	 * Програмыг эхлүүлэх функц 
	 */
	public void run() {
		logService = new LogServiceImpl();
		writeLogToFile(1);
		service.run();
	}

	/**
	 * Чат сервер дээр хадгалах
	 * 
	 * @param content
	 *            чатын агуулга
	 * @param date
	 *            чат бичсэн огноо
	 * @param chatId
	 *            чат id
	 */ 
	public void saveChatEntry(String content, String date, String chatId) {
		
		User user = userService.findByChatId(chatId);
		if (user != null) {
			ChatEntry chatEntry = new ChatEntry();
			chatEntry.setContent(content);
			chatEntry.setTimestamp(date);
			Chat chat = new Chat();
			chat.setId(Integer.parseInt(chatId));
			chatEntry.setChat(chat);
			chatEntryService.save(chatEntry);

			JSONObject obj = new JSONObject();
			obj.put("content", content);
			obj.put("date", date);
			obj.put("chatId", chatId);
			obj.put("name", user.getName());
			service.sendResult(3, obj.toJSONString());
			logService.writeLog("info", user.getName() + " чат бичлээ. Утга: " + content);
		} else {
			service.sendResult(0, "Хэрэглэгч олдсонгүй!");
			logService.writeLog("warning", "Энэ хэрэглэгч нэвтрээгүй байна.");
		}
	}

	/**
	 * Хэрэглэгч бүртгэх
	 * 
	 * @param name
	 *            хэрэглэгчийн нэр
	 * @param password
	 *            нууц үгs
	 */
	public void register(String name, String password) {
		if (userService.findByName(name) == null) {
			User user = new User();
			user.setName(name);
			user.setPassword(password);
			userService.save(user);
			service.sendResult(2, "Амжилттай!");
		} else {
			service.sendResult(0, "Бүртгэлтэй хэрэглэгч байна!");
		}
	}

	/**
	 * Хэрэглэгч нэвтрэх
	 * 
	 * @param name
	 *            хэрэглэгчийн нэр
	 * @param password
	 *            Хэрэглэгчийн нууц үг
	 */
	public void login(String name, String password) {
		User user = userService.findByNameAndPassword(name, password);
		if (user == null) {
			logService.writeLog("warning",
					"Буруу өгөгдөл оруулсан. Оруулсан нэр: " + name + " Оруулсан нууц үг : " + password);
			service.sendResult(0, "Таны нэр эсвэл нууц үг буруу байна!");

		} else {
			JSONObject object = new JSONObject();
			object.put("id", user.getId());
			object.put("name", user.getName());
			object.put("password", user.getPassword());
			object.put("chatId", user.getChat().getId());
			service.sendResult(1, object.toJSONString());;
			service.init(user.getName());
			logService.writeLog("info", "Хэрэглэгч " + user.getName() + " нэвтэрлээ");
		}
	}

	/**
	 * Лог бичих
	 * 
	 * @param logType
	 *            төрөл
	 */
	public void writeLogToFile(int logType) {
		switch (logType) {
		case 1:
			logService.writeLog("info", "Сервер ажиллаж эхэллээ");
			break;
		case 2:
			logService.writeLog("info", "Клиент сэрвэрт орж ирлээ");
			break;
		case 3:
			logService.writeLog("info", "Клиент серверээс гарлаа");
			break;
		case 4:
			logService.writeLog("error", "Сокэт эхлэхэд алдаа гарлаа");
			break;
		case 5:
			logService.writeLog("error", "Сокэт ажиллах үед алдаа гарлаа");
			break;
		case 6:
			logService.writeLog("error", "Оролт гаралтын алдаа гарлаа");
			break;
		case 7:
			logService.writeLog("error", "Өгөгдөл хөрвүүлэхэд алдаа гарлаа");
			break;
		case 8:
			logService.writeLog("error", "Сокэт-ыг зогсооход алдаа гарлаа");
			break;
		default:
			logType = 0;
			break;
		}
	}
	
	public void close() {
		userService.close();
		chatEntryService.close();
		logService.close();
	}
}
