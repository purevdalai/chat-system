package chinggis.systems.service;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LogServiceImpl implements LogService{
	
	Logger logger;  
    FileHandler fileHandler;
    
    public LogServiceImpl() {
    	logger = Logger.getLogger(LogServiceImpl.class.getName());
    }
    
    @Override
    public void writeLog(String logType, String data) {
    	try {
    		fileHandler = new FileHandler("ChatLog.log");  
    		logger.addHandler(fileHandler);
    		SimpleFormatter formatter = new SimpleFormatter();  
    		fileHandler.setFormatter(formatter);  
    		
    		if(logType.equals("info"))
    			logger.info(data);
    		if(logType.equals("warning"))
    			logger.warning(data);
    		if(logType.equals("error"))
    			logger.warning(data);

    	} catch (SecurityException e) {  
    		System.out.println(e.getMessage());
    		e.printStackTrace();  
    	} catch (IOException e) {  
    		System.out.println("log бичих үед алдаа гарлаа");
    		e.printStackTrace();  
    	} 
    }

	@Override
	public void close() {
		fileHandler.close();
	}
}
