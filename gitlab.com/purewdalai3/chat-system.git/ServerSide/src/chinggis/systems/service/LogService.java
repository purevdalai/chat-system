package chinggis.systems.service;

public interface LogService {
	/**
	 * Лог бичих
	 * 
	 * @param logtype
	 *            төрөл
	 * @param data
	 *            өгөгдөл
	 */
	void writeLog(String logType, String data);
	
	/**
	 * Log file хаах
	 * */
	void close();
}
