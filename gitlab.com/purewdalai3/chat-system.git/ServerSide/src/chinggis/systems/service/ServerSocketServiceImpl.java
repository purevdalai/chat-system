
package chinggis.systems.service;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;

import chinggis.systems.controller.ServerController;

public class ServerSocketServiceImpl implements MainService {

	private ServerSocket listener;
	private ServerController controller;
	private Socket socket;
	private Handler handler;
	private static HashSet<PrintWriter> writers = new HashSet<PrintWriter>();
	private static HashSet<String> users = new HashSet<String>();

	public ServerSocketServiceImpl() {
	}

	public ServerSocketServiceImpl(int port, ServerController controller) {
		this.controller = controller;
		connect(port);
	}

	@Override
	public void run() {
		try {
			while (true) {
				socket = listener.accept();
				handler = new Handler(socket, controller);
				handler.start();
				controller.writeLogToFile(2);
			}
		} catch (IOException e) {
			controller.writeLogToFile(6);
			e.printStackTrace();
		} finally {
			try {
				listener.close();
				controller.close();
			} catch (IOException e) {
				controller.writeLogToFile(6);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void connect(int port) {
		try {
			listener = new ServerSocket(port);
		} catch (IOException e) {
			controller.writeLogToFile(6);
			e.printStackTrace();
		}
	}

	@Override
	public void sendResult(int type, String message) {
		JSONObject object = new JSONObject();
		object.put("type", type);
		object.put("data", message);
		handler.sendResult(object.toJSONString());
	}

	@Override
	public void sendLoginResult(int type, String data) {
		JSONObject obj = new JSONObject();
		obj.put("type", type);
		obj.put("data", data);
		handler.sendResult(obj.toJSONString());
	}

	@Override
	public void init(String new_name) {
		if (!users.contains(new_name)) {
			JSONObject new_user = new JSONObject();
			new_user.put("type", 5);
			new_user.put("data", new_name);
			handler.sendGlobalMessage(new_user.toJSONString());
			users.add(new_name);
		}

		if (users.size() > 0) {
			Gson gson = new Gson();
			Set<String> set = users;
			String result[] = new String[users.size()];
			int index = 0;
			for (String s : set) {
				if (new_name != s)
					result[index] = s;
				index++;
			}
			String data = gson.toJson(result);
			JSONObject usernames = new JSONObject();
			usernames.put("type", 4);
			usernames.put("data", data);
			handler.sendResult(usernames.toJSONString());
		}
	}

	@Override
	public void sendGlobalResult(int type, String message) {
		JSONObject object = new JSONObject();
		object.put("type", type);
		object.put("data", message);
		handler.sendGlobalMessage(object.toJSONString());
	}

	private static class Handler extends Thread {
		private Socket socket;
		private DataInputStream dataInput;
		private ServerController controller;
		private PrintWriter printWriter;

		public Handler(Socket socket, ServerController controller) {
			this.socket = socket;
			this.controller = controller;
		}

		/**
		 * Хэрэглэгчид хариу илгээх
		 * 
		 * @param result
		 *            үр дүн
		 */
		private void sendResult(String result) {
			printWriter.println(result);
			printWriter.flush();
		}

		/**
		 * Бүх хэрэглэгчидэд хариу илгээх
		 * 
		 * @param result
		 *            үр дүн
		 */
		private void sendGlobalMessage(String result) {
			for (PrintWriter writer : writers) {
				writer.println(result);
				writer.flush();
			}
		}

		public void run() {
			try {
				dataInput = new DataInputStream(socket.getInputStream());
				printWriter = new PrintWriter(socket.getOutputStream(), true);
				writers.add(printWriter);

				boolean done = false;
				while (!done) {
					byte messageType = dataInput.readByte();
					JSONParser parser = new JSONParser();
					switch (messageType) {
					case 1:
						String login_data = dataInput.readUTF();
						JSONObject login_json = (JSONObject) parser.parse(login_data);
						String login_name = login_json.get("name").toString();
						String login_password = login_json.get("password").toString();
						controller.login(login_name, login_password);
						break;
					case 2:
						String register_data = dataInput.readUTF();
						JSONObject register_json = (JSONObject) parser.parse(register_data);
						String register_name = register_json.get("name").toString();
						String register_password = register_json.get("password").toString();
						controller.register(register_name, register_password);
						break;
					case 3:
						String chat_data = dataInput.readUTF();
						JSONObject chat_obj = (JSONObject) parser.parse(chat_data);
						Date now = new Date();
						String chat_content = chat_obj.get("content").toString();
						String chat_id = chat_obj.get("chatId").toString();
						controller.saveChatEntry(chat_content, now.toString(), chat_id);
						break;
					default:
						done = true;
						break;
					}
				}
			} catch (IOException e) {
				controller.writeLogToFile(6);
				e.printStackTrace();
			} catch (ParseException e) {
				controller.writeLogToFile(7);
				e.printStackTrace();
			} finally {
				try {
					socket.close();
					writers.remove(printWriter);
					printWriter.close();
					dataInput.close();
					controller.writeLogToFile(3);
				} catch (IOException e) {
					controller.writeLogToFile(6);
					e.printStackTrace();
				}
			}
		}
	}
}
