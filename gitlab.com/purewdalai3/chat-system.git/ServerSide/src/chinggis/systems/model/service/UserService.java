package chinggis.systems.model.service;

import chinggis.systems.model.entity.User;

public interface UserService {

	/**
	 * Шинэ хэрэглэгч үүсгэх
	 * 
	 * @param user
	 *            хэрэглэгч [нэр , нууц үг]
	 */
	void save(User user);

	/**
	 * Хэрэглэгчийг нэр , нууц үгээр хайх 
	 * 
	 * @param name
	 *            нэр
	 * @param password
	 *            нууц үг
	 * @return хэрэглэгч [id , нэр , нууц үг , chat id]
	 */
	User findByNameAndPassword(String name, String password);

	/**
	 * Хэрэглэгчийн нэрээр хайх
	 * 
	 * @param name
	 *            нэр
	 * @return хэрэглэгч [id , нэр , нууц үг , chat id]
	 */
	User findByName(String name);

	/**
	 * Хэрэглэгчийг чат дугаараар хайх
	 * 
	 * @param chatId
	 *            чатын дугаар
	 * @return хэрэглэгч [id , нэр , нууц үг , chat id]
	 */
	User findByChatId(String chatId);

	/**
	 * DB-с салгах
	 * */
	void close();
}
