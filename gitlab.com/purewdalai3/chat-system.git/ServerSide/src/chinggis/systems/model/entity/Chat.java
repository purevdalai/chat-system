package chinggis.systems.model.entity;

import java.io.Serializable;
import javax.persistence.*;

import chinggis.systems.model.entity.ChatEntry;
import chinggis.systems.model.entity.User;

import java.util.Collection;

@Entity
@Table(name = "Chat")
public class Chat implements Serializable {

	private static final long serialVersionUID = 1L;

	public Chat() {
	}

	@Id
	@GeneratedValue
	private long id;
	@OneToMany(mappedBy = "chat")
	private Collection<User> user;
	@OneToMany(mappedBy = "chat")
	private Collection<ChatEntry> chatEntry;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Collection<User> getUser() {
	    return user;
	}

	public void setUser(Collection<User> param) {
	    this.user = param;
	}

	public Collection<ChatEntry> getChatEntry() {
	    return chatEntry;
	}

	public void setChatEntry(Collection<ChatEntry> param) {
	    this.chatEntry = param;
	}

}