package chinggis.systems.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import chinggis.systems.model.entity.Chat;

import javax.persistence.ManyToOne;

@Entity
@Table(name = "User")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	public User() {
	}

	@Id
	@GeneratedValue
	private long id;
	private String name;
	private String password;
	@ManyToOne
	private Chat chat;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String param) {
		this.name = param;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String param) {
		this.password = param;
	}

	public Chat getChat() {
	    return chat;
	}

	public void setChat(Chat param) {
	    this.chat = param;
	}

}