package chinggis.systems.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import chinggis.systems.model.ChatEntry;
import chinggis.systems.model.Response;
import chinggis.systems.model.User;
import chinggis.systems.service.MainService;
import chinggis.systems.view.ChatView;
import chinggis.systems.view.LoginView;
import chinggis.systems.view.RegisterView;

public class ClientController {

	MainService service;
	LoginView loginView;
	RegisterView registerView;
	ChatView chatView;
	JSONParser parser;
	User user;
	Thread readThread;

	/**
	 * Байгуулагч функц
	 */
	public ClientController() {
	}

	/**
	 * Class -д ашиглагдах обьектуудыг олгоно.
	 * 
	 * @param service
	 *            буюу MainService сервисийн обьект
	 * @param loginView
	 *            хэрэглэгч нэвтрэх цонхны обьект
	 * @param registerView
	 *            хэрэглэгч бүртгэх цонхны обьект
	 * @param chatView
	 *            хэрэглэгчийн чат цонхны обьект
	 */
	public void init(MainService service, LoginView loginView, RegisterView registerView, ChatView chatView) {
		this.service = service;
		this.loginView = loginView;
		this.registerView = registerView;
		this.chatView = chatView;
		this.parser = parser = new JSONParser();
	}

	/**
	 * Програмын обьектуудыг үүссэн эсэхийг шалган програмыг ажилуулах функц
	 */
	public void run() {
		this.buildLoginView();
	}

	/**
	 * Нэвтрэх функц , сервер лүү нэвтрэх хүсэлт илгээн серверээс хариу аван чат
	 * цонх үүсгэнэ.
	 * 
	 * @param userName
	 *            хэрэглэгчийн нэр
	 * @param password
	 *            нууц үг
	 */
	public void login(String userName, String password) {
		Response response = service.loginRequest(userName, password);
		if (response.getType().equals("0")) {
			loginView.showErrorMsg(response.getJson());
			return;
		}
		try {
			JSONObject object = (JSONObject) parser.parse(response.getJson());
			user = new User(object.get("name").toString(), object.get("password").toString(),
					Integer.parseInt(object.get("chatId").toString()));
		} catch (ParseException e) {
			e.printStackTrace();
			return;
		}
		loginView.closeView();
		buildChatView();
		readStream();
	}

	/**
	 * Бүртгэх функц , сервер лүү бүртгүүлэх хүсэлт илгээн хариуг аван нэвтрэх
	 * цонхийг харуулна.
	 *  
	 * @param userName
	 *            хэрэглэгчийн нэр
	 * @param password
	 *            нууц үг
	 */
	public void register(String userName, String password) {
		Response response = service.registerRequest(userName, password);
		if (response.getType().equals("0")) {
			registerView.showErrorMessage(response.getJson());
		} else {
			registerView.closeView();
			buildLoginView();
		}
	}

	/**
	 * Сервер лүү хэрэглэгчийн бичсэн чатыг илгээх
	 * 
	 * @param content
	 *            чатын агуулга
	 */
	public void sendChatEntry(String content) {
		ChatEntry chatEntry = new ChatEntry();
		chatEntry.setContent(content);
		chatEntry.setChatId(user.getChatId());
		service.sendChatEntry(chatEntry);
	}

	/**
	 * Нэвтрэх цонх үүсгэх
	 */
	public void buildLoginView() {
		loginView.buildView();
	}

	/**
	 * Бүртгэх цонх үүсгэх
	 */
	public void buildRegisterView() {
		registerView.buildView();
	}

	/**
	 * Чат цонх үүсгэх
	 */
	public void buildChatView() {
		chatView.buildView();
	}

	/**
	 * Чат цонх ажиллаж эхлэх үед серверээс зогсолтгүй хариу унших функц
	 */
	private void readStream() {
		readThread = new Thread(() -> {
			
			while (true) {
				Response response = service.readStream();
				if (response != null && response.getType().equals("3"))
					chatView.loadChatEntries(response);
				if (response != null && response.getType().equals("4"))
					chatView.loadUsers(response);
				if (response != null && response.getType().equals("5"))
					chatView.loadUser(response);
				if (response != null && response.getType().equals("6"))
					chatView.loadHistory(response);
			}
		});
		readThread.start();
	}

	public void close() {
		readThread.stop();
		readThread.interrupt();
		service.close();
	}
}
