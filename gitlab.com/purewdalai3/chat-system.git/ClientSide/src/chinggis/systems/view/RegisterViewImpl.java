package chinggis.systems.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import chinggis.systems.controller.ClientController;
import chinggis.systems.view.ui.RegisterWindow;

public class RegisterViewImpl implements RegisterView {

	private ClientController controller;
	private RegisterWindow registerWindow;
	private String errorMessage;

	public RegisterViewImpl() {
	}

	public RegisterViewImpl(ClientController controller) {
		this.controller = controller;
		this.registerWindow = new RegisterWindow();
	}

	@Override
	public void buildView() {
		this.registerWindow.buildView();
		addListerner();
	}

	private void addListerner() {
		ActionListener listenerRegister = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				errorMessage = "";

				String username = registerWindow.getUserName().trim();
				String password = registerWindow.getPassword().trim();
				String verificationPassword = registerWindow.getVerificationPassword().trim();

				if (validation(username, password, verificationPassword)) {
					controller.register(username, password);
				} else {
					sendErrors(username, password, verificationPassword);
					showErrorMessage(errorMessage);
				}
				reset();
			}
		};
		this.registerWindow.addRegisterButtonClickListener(listenerRegister);
	}

	private boolean validation(String name, String pass, String pass2) {
		if (name.equals("") || pass.equals("") || pass2.equals("") || !pass.equals(pass2)) {
			return false;
		}
		return true;
	}
	
	private void reset() {
		registerWindow.setUserName("");
		registerWindow.setPassword("");
		registerWindow.setVerificationPassword("");
	}

	private void sendErrors(String name, String pass, String pass2) {
		if (name.equals("")) {
			errorMessage += "Та нэрээ оруулна уу! ";
		}
		if (pass.equals("")) {
			errorMessage += "Та нууц үгээ оруулна уу! ";
		}
		if (!pass.equals(pass2)) {
			errorMessage += "Таны нууц үгнүүд хоорондоо зөрүүтэй байна! ";
		}
	}

	@Override
	public void showErrorMessage(String errorMessage) {
		registerWindow.setErrorMessage(errorMessage);
	}

	@Override
	public void closeView() {
		registerWindow.closeWindow();
	}
}
