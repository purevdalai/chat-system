package chinggis.systems.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;

import chinggis.systems.controller.ClientController;
import chinggis.systems.model.Response;
import chinggis.systems.view.ui.ChatWindow;

public class ChatViewImpl implements ChatView {

	ClientController controller;
	ChatWindow chatWindow;
	JSONParser parser;

	public ChatViewImpl() {
	}

	public ChatViewImpl(ClientController controller) {
		this.controller = controller;
		this.chatWindow = new ChatWindow();
		this.parser = new JSONParser();
	}

	@Override
	public void buildView() {
		this.chatWindow.buildView();
		addActionListener();
	}

	private void addActionListener() {
		ActionListener listenerSend = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String content = chatWindow.getContent();
				if (!content.trim().equals("")) {
					controller.sendChatEntry(content);
					chatWindow.setContent("");
				}
			}
		};
		ActionListener fieldListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String content = chatWindow.getContent();
				if (!content.trim().equals("")) {
					controller.sendChatEntry(content);
					chatWindow.setContent("");
				}
			}
		};

		WindowAdapter windowListener = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				controller.close();
				e.getWindow().dispose();
			}
		};

		chatWindow.addActionListenerButtonSend(listenerSend);
		chatWindow.addTextFieldListener(fieldListener);
		chatWindow.addCloseListener(windowListener);
	}

	@Override
	public void loadHistory(Response response) {
		try {
			JSONArray jsonArray = (JSONArray) parser.parse(response.getJson());
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject obj = (JSONObject) jsonArray.get(i);
				String content = obj.get("content").toString();
				String date = obj.get("timestamp").toString();
				String userName = obj.get("userName").toString();
				chatWindow.addChatEntry(content, date, userName);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void loadChatEntries(Response response) {
		try {
			JSONObject obj = (JSONObject) parser.parse(response.getJson());
			String content = obj.get("content").toString();
			String date = obj.get("date").toString();
			String userName = obj.get("name").toString();
			chatWindow.addChatEntry(content, date, userName);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void loadUsers(Response response) {
		Gson gson = new Gson();
		String[] result_str = gson.fromJson(response.getJson(), String[].class);
		for (String res : result_str) {
			chatWindow.addUsername(res);
		}
	}

	@Override
	public void loadUser(Response response) {
		chatWindow.addUsername(response.getJson());
	}
}
