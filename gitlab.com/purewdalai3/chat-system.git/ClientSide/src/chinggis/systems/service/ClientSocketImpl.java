package chinggis.systems.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import chinggis.systems.model.ChatEntry;
import chinggis.systems.model.Response;

public class ClientSocketImpl implements MainService {

	private Socket socket;
	private DataOutputStream dataOutput;
	private BufferedReader bufferedReader;
	private JSONParser parser;

	public ClientSocketImpl() {
	}

	public ClientSocketImpl(String serverAddress, int port) {
		init(serverAddress, port);
	}

	@Override
	public Response loginRequest(String userName, String password) {
		JSONObject obj = new JSONObject();
		obj.put("name", userName);
		obj.put("password", password);
		sendMessage(1, obj.toJSONString());
		return readStream();
	}

	@Override
	public Response readStream() {
		String line = null;
		try {
			line = bufferedReader.readLine();
			JSONObject object = (JSONObject) parser.parse(line);
			String type = object.get("type").toString();
			String json = object.get("data").toString();
			return new Response(type, json);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Response registerRequest(String userName, String password) {
		JSONObject obj = new JSONObject();
		obj.put("name", userName);
		obj.put("password", password);
		sendMessage(2, obj.toJSONString());
		return readStream();
	}

	@Override
	public void sendChatEntry(ChatEntry chatEntry) {
		JSONObject obj = new JSONObject();
		obj.put("content", chatEntry.getContent());
		obj.put("chatId", chatEntry.getChatId());
		sendMessage(3, obj.toJSONString());
	}

	/**
	 * Хэрэглэгч талаас сервер лүү комманд мессеж явуулах
	 * 
	 * @param type
	 *            төрөл
	 * @param content
	 *            агуулга
	 */
	private void sendMessage(int type, String content) {
		try {
			dataOutput.writeByte(type);
			dataOutput.writeUTF(content);
			dataOutput.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void init(String serverAddress, int port) {
		try {
			socket = new Socket(serverAddress, port);
			dataOutput = new DataOutputStream(socket.getOutputStream());
			bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			parser = new JSONParser();
			socket.setSoTimeout(10000);
			System.out.println("Server -тэй амжилттай холбогдлоо!");
		} catch (IOException e) {
			System.out.println("Server -тэй холбогдож чадсангүй!");
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		try {
			sendMessage(0, "");
			dataOutput.close();
			bufferedReader.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
