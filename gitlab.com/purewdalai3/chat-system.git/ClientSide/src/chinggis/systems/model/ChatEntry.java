package chinggis.systems.model;

public class ChatEntry {
	String content;
	String date;
	int chatId;
	
	public ChatEntry() {}
	
	public ChatEntry(String content , String date , int id) {
		this.content = content;
		this.date = date;
		this.chatId = id;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public void setTimeStamp(String date) {
		this.date = date;
	}

	public void setChatId(int id) {
		this.chatId = id;
	}
	
	public String getContent() {
		return content;
	}
	
	public String getDate() {
		return date;
	}
	
	public int getChatId() {
		return chatId;
	}

}
